package com.isystem.linkersback.exceptions

class LinkersValidationException(var erroCode:String): Exception(erroCode) {
    var params: Array<out String> = arrayOf()

    constructor(erroCode: String, vararg params: String) : this(erroCode) {
        this.params = params
    }

}