package com.isystem.linkersback.resources

import com.isystem.linkersback.exceptions.LinkersValidationException
import javax.inject.Singleton
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider
import javax.inject.Inject


@Provider
@Singleton
class GeralHandleException : ExceptionMapper<Throwable> {

    @Inject
    private lateinit var messages: Messages

    private companion object {
        private const val ERRO_GERAL = "erro.geral"
    }

    override fun toResponse(exception: Throwable): Response {
        return Response.status(getStatus(exception))
                .entity(getEntity(exception))
                .type(MediaType.APPLICATION_JSON).build()
    }

    private fun getStatus(exception: Throwable): Response.Status {
        if (exception is LinkersValidationException)
            return Response.Status.BAD_REQUEST
        return Response.Status.INTERNAL_SERVER_ERROR
    }

    private fun getEntity(exception: Throwable): MessageTO {
        if (exception is LinkersValidationException)
            return messages.getMessageTO(exception)
        return messages.getMessageTO(ERRO_GERAL)
    }
}