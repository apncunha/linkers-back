package com.isystem.linkersback.resources

import com.isystem.linkersback.exceptions.LinkersValidationException
import org.springframework.context.support.MessageSourceAccessor
import org.springframework.context.MessageSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

class MessageTO(var msg: String?)

@Component
class Messages {

    @Autowired
    private lateinit var messageSource: MessageSource

    private var accessor: MessageSourceAccessor? = null

    @PostConstruct
    private fun init() {
        accessor = MessageSourceAccessor(messageSource)
    }

    internal operator fun get(code: String, vararg params: String): String? {
        return accessor!!.getMessage(code, params)
    }

    internal fun getMessageTO(code: String, vararg params: String): MessageTO {
        val msg = get(code, *params)
        return if (msg == null) MessageTO(code) else MessageTO(msg)
    }

    internal fun getMessageTO(exception: LinkersValidationException): MessageTO {
        val msg = get(exception.erroCode, *exception.params)
        return if (msg == null) MessageTO(exception.message) else MessageTO(msg)
    }
}
