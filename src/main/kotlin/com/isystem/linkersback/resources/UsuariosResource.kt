package com.isystem.linkersback.resources

import com.isystem.linkersback.business.UsuariosBO
import com.isystem.linkersback.entitys.Usuarios
import com.isystem.linkersback.exceptions.LinkersValidationException
import com.isystem.linkersback.tos.UsuariosTO
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status

@Path("/linkers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class UsuariosResource {

    @Inject
    lateinit var usuarioBO: UsuariosBO

    private companion object {
        private const val ERRO_NULL = "erro.null"
        private const val USUARIO = "usuário"
    }

    /**
     * Método de exemplo para listagem
     * http://localhost:3000/linkers?id=5c05fe675e07b8200eb3bc39
     *
     * */
    @GET
    fun buscarUsuarioPorId(@QueryParam("id") id: String): Response {
        val usuario = usuarioBO.buscarPorId(id)
        usuario ?: return Response.status(Status.NO_CONTENT).build()
        return Response.ok(usuario).build()
    }

    /**
     * Método de exemplo pra salvar um usuário
     * http://localhost:3000/linkers
     * body: {
     *      "nome": "Heitor Cunha",
     *      "email": "heitor.cunha@gmail.com",
     *      "login": "heitor.cunha",
     *      "senha": "123",
     *   }
     * */
    @POST
    fun salvarUsuario(usuarioTO: UsuariosTO?): Response {
        if (!this.obrigatoriosParaSalvar(usuarioTO))
            throw LinkersValidationException(ERRO_NULL, USUARIO)

        val usuario: Usuarios = usuarioBO.salvar(usuarioTO!!.entity())
        return Response.ok(usuario).build()
    }

    private fun obrigatoriosParaSalvar(usuarioTO: UsuariosTO?): Boolean {
        if (usuarioTO?.nome !== null && usuarioTO.email !== null
                && usuarioTO.login !== null && usuarioTO.senha !== null) {
            return true
        }
        return false
    }
}