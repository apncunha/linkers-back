package com.isystem.linkersback.resources

import com.sun.xml.internal.ws.handler.HandlerException
import org.glassfish.jersey.server.ResourceConfig
import org.springframework.stereotype.Component

@Component
class JerseyConfig : ResourceConfig() {
    init {
        packages("com.isystem.linkersback.resources")
        registerClasses(HandlerException::class.java, UsuariosResource::class.java)
    }
}