package com.isystem.linkersback.tos

import com.isystem.linkersback.entitys.Usuarios
import org.bson.types.ObjectId
import org.modelmapper.ModelMapper

data class AmizadeTO(var id: ObjectId?, var amigo: UsuariosTO?, var amigoTelefones: List<TelefonesTO>?,
                     var meusTelefones: List<TelefonesTO>?)

data class TelefonesTO(var id: ObjectId?, var numero: String?, var descricao: String?)

data class UsuariosTO (var id:ObjectId?, var nome: String?, var email: String?, var login: String?, var senha: String?,
                       var telefones: List<TelefonesTO>?, var amigos: List<AmizadeTO>?) {
    fun entity() : Usuarios {
        return ModelMapper().map(this, Usuarios::class.java)
    }
}

fun convert(entity: Usuarios) : UsuariosTO{
    val modelMapper = ModelMapper()
    modelMapper.createTypeMap(Usuarios::class.java, UsuariosTO::class.java)
    return modelMapper.map(entity, UsuariosTO::class.java)
}

fun convertList(usuarios: List<Usuarios>) : List<UsuariosTO> {
    return usuarios?.map{ convert(it) }
}

fun convertEntityList(usuariosTO : List<UsuariosTO>) : List<Usuarios> {
    return usuariosTO?.map{ it.entity() }
}