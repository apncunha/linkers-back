package com.isystem.linkersback

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LinkersBackApplication

fun main(args: Array<String>) {
    runApplication<LinkersBackApplication>(*args)
}
