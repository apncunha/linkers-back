package com.isystem.linkersback.business

import com.isystem.linkersback.entitys.Usuarios
import com.isystem.linkersback.repository.UsuariosRepository
import org.bson.types.ObjectId
import org.springframework.stereotype.Component
import java.util.*
import javax.inject.Inject

@Component
class UsuariosBO {

    @Inject
    lateinit var usuarioRepository: UsuariosRepository

    fun buscarPorId(id: String): Usuarios? {
        var usuarioOptional: Optional<Usuarios> = usuarioRepository.findById(ObjectId(id))
        return if (usuarioOptional.isPresent) usuarioOptional.get() else null
    }

    fun salvar(usuario: Usuarios): Usuarios {
        return this.usuarioRepository.save(usuario)
    }

}