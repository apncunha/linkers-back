package com.isystem.linkersback.repository

import com.isystem.linkersback.entitys.Usuarios
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface UsuariosRepository : MongoRepository<Usuarios, ObjectId>