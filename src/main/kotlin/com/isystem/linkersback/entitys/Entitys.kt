package com.isystem.linkersback.entitys

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "postagens")
data class Postagens(@Id var id: ObjectId? = null, var texto: String = "", var curtidas: Long = 0,
                     @DBRef(lazy = true) var respostas: List<Postagens>? = listOf())

@Document(collection = "amigos")
data class Amizade(@Id var id: ObjectId? = null, @DBRef var amigo: Usuarios? = null,
                   var amigoTelefones: List<Usuarios.Telefones> = listOf(),
                   val meusTelefones: List<Usuarios.Telefones> = listOf())

@Document(collection = "usuarios")
data class Usuarios(@Id var id: ObjectId? = null, @Indexed var nome: String? = "", var email: String? = "",
                    var login: String? = "", var senha: String? = "",
                    var telefones: List<Telefones>? = listOf(),
                    @DBRef var amigos: List<Amizade>? = listOf(),
                    @DBRef(lazy = true) var postagens: List<Postagens>? = listOf()) {
    data class Telefones(@Id var id: ObjectId? = ObjectId(), var numero: String = "", var descricao: String = "")
}
